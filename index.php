<?php
include_once ("game.php");
include_once ("DoubleFixed.php");
include_once ("RemoveUnique.php");
include_once ("gamedetail.php");

if (isset($_REQUEST['game']))
{
	$solver = "simple";
	if (isset($_REQUEST['solver'])) $solver = $_REQUEST['solver'];
	
	switch ($solver)
	{
		case "simple":
			$game = new Game($_REQUEST['game'].".txt");
			break;
		case "cleverer":
			$game = new RemoveUnique($_REQUEST['game'].".txt"); 
			break;
		default:
			$game = new Game($_REQUEST['game'].".txt");
			break;
	}
    $game->solve();
    $game->show_cells();
}
else
{
	//echo "Please specify game name using variable \"filename\"";
	echo "
	Usable variable:
	<li>game = <i>name of the game</i>
	<li>solver = [simple | cleverer]
	<br><br>
	Or you can click on the links below:<br><br>
	";
	$allGames = new GameDetail();
	$allGames->printAllGames();
}

?>