<?php

/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of RemoveUnique
 *
 * @author Douglas
 */
final class RemoveUnique extends Game {
    function __construct($filename = "") {
        parent::__construct($filename);
        $this->_passes = array("DoubleFixed", "RemoveUnique");
    }

    function solve_logic() {
        for ($i = 0; $i < 9; $i++) {
            $this->_solve($this->_columns[$i]->ids());
            $this->_solve($this->_rows[$i]->ids());
            $this->_solve($this->_squares[$i]->ids());
        }
    }
    
    private function _solve($ids){
        $occurrence = array();
        
        foreach ($ids as $id) {
            $posi = $this->_cells[$id]->possibility();
            for ($i = 1; $i <= 9; $i++) {
                if (in_array($i, $posi)){
                    $occurrence[$i]++;
                }
            }
        }
        
        foreach($occurrence as $key=>$o){
            if ($o==1){
                foreach ($ids as $id) {
                    $posi = $this->_cells[$id]->possibility();
                    if(in_array($key, $posi)){
                        $this->_cells[$id]->setAnswer($key);
                    }
                }
            }
        }
    }
}

?>
