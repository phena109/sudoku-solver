<?php
class GameDetail
{
	private $_dir;
	
	public function __construct($path = "./game/")
	{
		if (!empty($path))
			$this->_dir = $path;			
	}
	
	public function load($filePath)
	{
		$fp = fopen($filePath, "r");
		
		if ($fp==null)
		{
			echo "File Open Failed\n<br>";
			return;
		}
		
		echo "Yes";
	}
	
	function printAllGames()
	{
		$allFiles = scandir($this->_dir);
		$allGames = array();
		
		echo "<table>\n";
		foreach ($allFiles as $file)
		{
			if (substr($file, -4) == ".txt")
			{
				$oneGame = substr($file, 0, -4);
				$allGames[] = "<a href=\"./index.php?game=".$oneGame."\">".$oneGame."</a>";
				?>
				<tr>
					<td style='border: solid 1px black'><?php echo $oneGame; ?></td>
					<td style='border: solid 1px black'><?php echo "<a href=\"./index.php?game=".$oneGame."\">Play (Simple Solver)</a>"; ?></td>
					<td style='border: solid 1px black'><?php echo "<a href=\"./index.php?solver=cleverer&game=".$oneGame."\">Play (Clever Solver)</a>"; ?></td>
					<td style='border: solid 1px black'><?php echo "<a href=\"./index.php?game=".$oneGame."\">Detail</a>"; ?></td>
				</tr>			
				<?php
			} 
		}
		echo "</table>\n";
	}
}

?>