<?php

include_once ("group.php");

class Square extends Group {

    static function SquareId($id) {
        $squares = array(
            "0" => array(0, 1, 2, 9, 10, 11, 18, 19, 20),
            "1" => array(3, 4, 5, 12, 13, 14, 21, 22, 23),
            "2" => array(6, 7, 8, 15, 16, 17, 24, 25, 26),
            "3" => array(27, 28, 29, 36, 37, 38, 45, 46, 47),
            "4" => array(30, 31, 32, 39, 40, 41, 48, 49, 50),
            "5" => array(33, 34, 35, 42, 43, 44, 51, 52, 53),
            "6" => array(54, 55, 56, 63, 64, 65, 72, 73, 74),
            "7" => array(57, 58, 59, 66, 67, 68, 75, 76, 77),
            "8" => array(60, 61, 62, 69, 70, 71, 78, 79, 80)
        );
        return $squares[$id];
    }
}

?>