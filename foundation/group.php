<?php

include_once ("inc.php");

/**
 * The group stores only id if the cells in a game
 * @author Douglas
 */
class Group
{
    protected $_cells = array();
    protected $_ids = array();
	
    /**
     * Define a group, can be a row, column or square
     * @param array $cells 
     */
	function __construct(&$cells, $ids)
	{
        $this->_cells = $cells;
        $this->_ids = $ids;
	}
	
    /**
     * short hand to determine if the group is solved
     * @return boolean 
     */
	function solved()
	{
		$flag = true;
        foreach ($this->_ids as $id){
            if (!$this->_cells[$id]->solved()){
                $flag = false;
            }
        }
        return $flag;
	}
    
    /**
     * Return indicies for other functions to reference to
     * @return array 
     */
    function ids(){
        return $this->_ids;
    }
	
    // <editor-fold defaultstate="collapsed" desc="Old Code">
        
//	/**
//	 * Sift out those with 2 possibilities must be in only 2 cells from other cells
//	 * @return true for some change is made, false for no change is made
//	 */
//	public function sift2()
//	{
//		$repeat = false;
//		$repeat_count = 0;
//		do
//		{
//			$repeat = false;
//			for ($i=0; $i<9; $i++)
//			{
//				for ($j=$i+1; $j<9; $j++)
//				{
//					// if 2 cells have the same 2 possibilities
//					if ($this->haveSame2Possibilities($i, $j))
//					{
//						for ($k=0; $k<9; $k++)
//						{				
//							// try not to touch the checking ones and those solved			
//							if ((($k!=$i)&&($k!=$j))&&(!$this->cells($k)->answer()))
//							{
//								$ip = $this->cells($i)->possibilities();
//								if ($this->cells($k)->setImpossibility($ip))
//								{
//									$repeat = true;	//repeat only when something changed
//									$repeat_count++;
//								}
//							}
//						}
//					}
//				}
//			}
//		} while ($repeat);
//		return ($repeat_count!=0);
//	}
//
//	/**
//	 * Sift out unique possibilties in the group
//	 * @return true for some change is made, false for no change is made
//	 */
//	public function sift3()
//	{
//		$flag = false;
//		for ($i=0; $i<9; $i++)
//		{
//			$id = -1;	// must be something not in the range 0~8
//			$count = 0;
//			for ($j=0; $j<9; $j++)
//			{
//				if (in_array($i, $this->cells($j)->possibilities()))
//				{
//					$count++;
//					if ($count>1) break;	// only 1 existense is needed
//					$id = $j;
//				}
//			}
//			if ($count==1)
//			{
//				if ($this->cells($id)->noOfPossibility()>1)
//				{
//					$this->cells($id)->setAnswer($i);
//					$flag = true;
//				}
//			}
//		}
//		return $flag;
//	}
//	
//	/**
//	 * (non-PHPdoc)
//	 * @see cells#cells()
//	 */
//	public function cells($id)
//	{
//		return $this->_game->cells($this->_cells[$id]);
//	}	
//	
//	/**
//	 * Check if a particular already filled in the set
//	 * @param $theNumber
//	 * @return bool
//	 */	
//	public function has($number)
//	{
//		for($i=0; $i<9; $i++)
//		{
//			if ($this->cells($i)->answer()==$number)
//			{
//				return true;
//			}
//		}
//		return false;
//	}
//
//	/**
//	 * Check if 2 cells have the same 2 possibilities
//	 * @param $id1
//	 * @param $id2
//	 * @return unknown_type
//	 */
//	public function haveSame2Possibilities($id1, $id2)
//	{
//		if ($this->cells($id1)->noOfPossibility()!=2) return false;
//		if ($this->cells($id2)->noOfPossibility()!=2) return false;
//		return ($this->cells($id1)->possibilities() == $this->cells($id2)->possibilities());
//	}
//	
//	/**
//	 * Check if 3 cells have the same 3 possibilities
//	 * @param $id1
//	 * @param $id2
//	 * @param $id3
//	 * @return unknown_type
//	 */
//	public function haveSame3Possibilities($id1, $id2, $id3)
//	{
//		if ($this->cells($id1)->noOfPossibility()!=2) return false;
//		if ($this->cells($id2)->noOfPossibility()!=2) return false;
//		if ($this->cells($id3)->noOfPossibility()!=2) return false;		
//		if ($this->cells($id1)->possibilities() != $this->cells($id2)->possibilities()) return false;
//		return ($this->cells($id2)->possibilities() == $this->cells($id3)->possibilities()); 		
//	}
    
    // </editor-fold>    
}

?>