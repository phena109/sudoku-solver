<?php

include_once ("group.php");

class Column extends Group {

    static function ColumnId($id) {
        $ids = array();
        for ($i = 0; $i < 9; $i++) {
            $ids[] = $i * 9 + $id;
        }
        return $ids;
    }
}

?>