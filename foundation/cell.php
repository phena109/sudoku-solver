<?php

include_once ("inc.php");

class Cell
{
	// All the possible values that the cell can have,
	// if 1 possibility left, it will be considers as solved
	protected $_possibility = array();
	
	protected $_location_r;
	protected $_location_c;
	protected $_location_s;
	
	protected $_id;

    /**
     * Define the cell
     * @param type $id 0~80, define where the cell belongs to
     * @param type $value 0 for unknown or other known answers range 1~9
     */
    function __construct($id, $value){                
		$this->_id = $id;		
        $this->presetValue($value);

		// Find out where the cell is locating
		$this->_location_c = $id % 9;				// only produce 0~8
		$this->_location_r = intval($id / 9);		// only produce 0~8
		
		$temp_c = intval($this->_location_c / 3);
		$temp_r = intval($this->_location_r / 3);
		
		$this->_location_s = $temp_c + $temp_r * 3;	// only produce 0~8
    }
    
    /**
     * Preset the value for the game for later evaluation
     * @param type $value
     */
	private function presetValue($value)
	{
		if (!is_numeric($value)) return;
        
		if (($value>0)&&($value<10))
		{
			$this->_possibility = array($value);
		}
		else if ($value == 0)
		{
			// Initialise the possibility of the cell
			for ($i = 1; $i <= 9; $i++)
			{
				$this->_possibility[] = $i;
			}
		}
	}
    
    /**
     * if the answer is known via some logic, this can set directly the value
     * @param int $value
     */
    function setAnswer($value){
		if (!is_numeric($value)) return;
        
		if (($value>0)&&($value<10))
		{
			$this->_possibility = array($value);
		}
    }
    
    /**
     * 0 for no answer. the rest is self-explanatory
     * @return int 
     */
    function answer(){
        if ($this->solved()){
            return $this->_possibility[0];
        }    
        return 0;
    }
    
    /**
     * to tell if the cell have only 1 possibility or not
     * @return boolean 
     */
    function solved(){
        return (count($this->_possibility)==1);
    }
    
    /**
     * list all the possibilities. can be just an array with 1 element
     * @return array 
     */
    function possibility(){
        return $this->_possibility;
    }
        
    /**
     * remove possibility, can be 1 number or array of numbers
     * @param type $value
     * @return boolean 
     */
    function removePossibility($value){
        $toRemove = array();
        if (is_array($value)){
            $toRemove = array_merge($toRemove,$value);
        }else{
            $toRemove[] = $value;
        }
        
        $old_count = count($this->_possibility);
        if ($old_count>1){
            $this->_possibility = array_diff($this->_possibility, $toRemove);
            
            // clear the array indicies
            $this->_possibility = array_values($this->_possibility);
        }
        $new_count = count($this->_possibility);
        
        // to tell if situation change
        return ($new_count < $old_count);
    }
}
?>