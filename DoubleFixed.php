<?php

/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of double_fixed
 *
 * @author Douglas
 */
final class DoubleFixed extends Game {
    function __construct($filename = "") {
        parent::__construct($filename);
        $this->_passes[] = "DoubleFixed";
    }
    
    function solve_logic() {
        for ($i = 0; $i < 9; $i++) {
            $this->_solve($this->_columns[$i]->ids());
            $this->_solve($this->_rows[$i]->ids());
            $this->_solve($this->_squares[$i]->ids());
        }   
    }
        
    private function _solve($ids){
        $toRemove = array();
        foreach ($ids as $id1){
            $posi1 = $this->_cells[$id1]->possibility();
            // find only cells with 2 possibilities
            if (count($posi1)!=2) continue;
            
            $count = 0;
            foreach ($ids as $id2){
                $posi2 = $this->_cells[$id2]->possibility();
                if (count($posi2)!=2) continue; // has to be with 2 possibilities
                if ($id1 == $id2) continue; // can't be the same cell
                if ($posi1 != $posi2) continue; // values much match, of cos
                $count++;
            }       
            
            // if there are repeating and not in the remove queue
            if (($count == 1) && (!in_array($posi1, $toRemove))){
                $toRemove[] = $posi1; // insert to the remove queue
            }
        }
        
        // for each cell in the group
        foreach ($ids as $id){
            // for each item in the remove queue
            foreach ($toRemove as $posi){
                // remove the only one with different values
                if ($this->_cells[$id]->possibility() != $posi){
                    $this->_cells[$id]->removePossibility($posi);
                }
            }
        }
    }
}

?>
