<?php

include_once ("inc.php");

class Game
{
    protected $_cells = array();
	protected $_rows = array();
	protected $_columns = array();
	protected $_squares = array();
    
    // for extendability
    protected $_passes = array();
	
    function __construct($filename="") {
		if ($filename != "") {
            $c = $this->load($filename);
        } else {
            $c = array(0, 0, 0, 0, 0, 0, 0, 0, 0,
                       0, 0, 0, 0, 0, 0, 0, 0, 0,
                       0, 0, 0, 0, 0, 0, 0, 0, 0,
                       0, 0, 0, 0, 0, 0, 0, 0, 0,
                       0, 0, 0, 0, 0, 0, 0, 0, 0,
                       0, 0, 0, 0, 0, 0, 0, 0, 0,
                       0, 0, 0, 0, 0, 0, 0, 0, 0,
                       0, 0, 0, 0, 0, 0, 0, 0, 0,
                       0, 0, 0, 0, 0, 0, 0, 0, 0);
        }

        for ($i = 0; $i < 81; $i++) {
            $this->_cells[] = new Cell($i, $c[$i]);
        }

        for ($i = 0; $i < 9; $i++) {
            $this->_columns[] = new Column($this->_cells, Column::ColumnId($i));
            $this->_rows[] = new Row($this->_cells, Row::RowId($i));
            $this->_squares[] = new Square($this->_cells, Square::SquareId($i));
        }         
    }
    
    /**
     * Clone the current game's cells value. For the purpose of multi-pass solving
     * @return Cell[] 
     */
    final function clone_cells() {
        return $this->_cells;
    }
    
    /**
     * Copy the cloned cell values from the given game
     * @param Game $game 
     */
    final  function get_clone_cells(&$game){
        $this->_cells = $game->clone_cells();
    }

	/**
	 * Load the game from the specific file structure
	 * @param $filename
	 * @return unknown_type
	 */
	final function load($filename)
	{
		// start load from a file
		$fp = fopen("./game/".$filename, "r");
		$read = "Douglas";
		$c = array();
		while ($read!==false)
		{
			$read = fgetc($fp);
			if (is_numeric($read))
			{
				$c[] = $read;
			}
		}
		fclose($fp);
		// finished load from a file		
		return $c;
	}
	
    /**
     * To tell if the game is solved or not
     * @return boolean 
     */
    final function solved(){
		for ($i=0; $i<81; $i++)
		{ 
            // if at least 1 is not solve, then the game is not solved
            if (!$this->_cells[$i]->solved()) return false;
		}
        return true;
    }
    
    /**
     * Solve obvious part of the game, ideal for beginning and ending of the game
     * @return boolean True for solved
     */
	final function simple_solve() {
        $flag = 1;
        while ($flag) {
            $flag = 0;
            for ($i    = 0; $i < 9; $i++) {
                // Remove impossible possiblilities of each cells              
                $flag += $this->_simple_solve($this->_columns[$i]->ids());
                $flag += $this->_simple_solve($this->_rows[$i]->ids());
                $flag += $this->_simple_solve($this->_squares[$i]->ids());
            }
        }

        return $this->solved();
    }
    
    /**
     * solve for a group
     * @param array $ids
     * @return int 
     */
    private function _simple_solve($ids){
        $used = array();  
        $flag = 0;
        foreach ($ids as $id){
            $used[] = $this->_cells[$id]->answer();
        }
        foreach ($ids as $id){
            $flag += $this->_cells[$id]->removePossibility($used);
        }  
        return $flag;
    }
	
	/**
	 * Wrapper for simple_solve
	 * @return boolean
	 */
    function solve()
	{
        for ($i = 0; ($i < 100) && (!$this->simple_solve()); $i++) {
            // to get the numeric result from other solver and so you get a simpler game
            foreach ($this->_passes as $p) {
                $pass = new $p();
                $pass->get_clone_cells($this);
                $pass->solve_logic();
                $this->get_clone_cells($pass);
            }
        }
    }
    
    function solve_logic(){
        return $this->simple_solve();
    }
    
	/**
	 * Quick and dirty solution to display the result
	 * @return unknown_type
	 */
    function show_cells()
	{
		$style = 'border: 1px #000 solid';
		echo "<table>\n";
			echo "<tr>";
				for ($i =0; $i<3; $i++)
				{
					echo "\n<td style='$style'>";
					$this->show_square($i);
					echo "</td>";
				}				
			echo "</tr>";
			
			echo "<tr>";
				for ($i =3; $i<6; $i++)
				{
					echo "\n<td style='$style'>";
					$this->show_square($i);
					echo "</td>";
				}	
			echo "</tr>";
			
			echo "<tr>";
				for ($i =6; $i<9; $i++)
				{
					echo "\n<td style='$style'>";
					$this->show_square($i);
					echo "</td>";
				}	
			echo "</tr>";
		echo "</table>\n";
	}
	
	private function show_square($id)
	{
		$style = 'border: 2px #0ff solid; width: 60px;';
		echo "<table>\n";
			echo "<tr>";
				for ($i =0; $i<3; $i++)
				{
					echo "\n<td style='$style'>";
//					$out = $this->squares($id)->cells($i)->possibilities();
                    $ids = Square::SquareId($id);
                    $out = $this->_cells[$ids[$i]]->possibility();
					foreach ($out as $o) echo $o." "; 
					echo "</td>";
				}				
			echo "</tr>";
			
			echo "<tr>";
				for ($i =3; $i<6; $i++)
				{
					echo "\n<td style='$style'>";
//					$out = $this->squares($id)->cells($i)->possibilities();
                    $ids = Square::SquareId($id);
                    $out = $this->_cells[$ids[$i]]->possibility();
					foreach ($out as $o) echo $o." "; 
					echo "</td>";
				}	
			echo "</tr>";
			
			echo "<tr>";
				for ($i =6; $i<9; $i++)
				{
					echo "\n<td style='$style'>";
//					$out = $this->squares($id)->cells($i)->possibilities();
                    $ids = Square::SquareId($id);
                    $out = $this->_cells[$ids[$i]]->possibility();
					foreach ($out as $o) echo $o." "; 
					echo "</td>";
				}	
			echo "</tr>";
		echo "</table>\n";
	}
}
?>